﻿using UnityEngine;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using System.Collections;
using System.Collections.Generic;

public class GameSparks_UserManager : MonoBehaviour {

    public static GameSparks_UserManager instance;

    [Header("Account Details")]
    public string userName;
    public string userId;
    public string facebookID;
    [Header("Score")]
    public int score;
    [Header("Currency/VG")]
    public int playerCurrency1;
    public int playerCurrency2;
    public int virtualItemsAvailable;

    UI_Manager ui;

    void Start()
    {
        instance = this;
        ui = GetComponent<UI_Manager>();

        Setup_MSG_Delegates();
        Get_AccountDetails();
    }

    void Setup_MSG_Delegates()
    {
        GameSparks.Api.Messages.AchievementEarnedMessage.Listener += Achievement_MessageHandler;
        GameSparks.Api.Messages.NewHighScoreMessage.Listener += HighScore_MessageHandler;
    }

    public void Get_AccountDetails()
    {
        //We send an AccountDetailsRequest
        new AccountDetailsRequest().Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log("Updated User Information");
                //We pass the details we want from our response to the function which will update our information

                GSData data = response.ExternalIds;
                
                //Debug.Log("!!!!!!!!!!!!"+ data.GetString("FB")); //NB -------Pull from Response

                UpdateGUI(response.DisplayName, response.UserId, data.GetString("FB"));//, response.ExternalIds.GetString("FB").ToString());
                playerCurrency1 = (int)response.Currency1;
                playerCurrency2 = (int)response.Currency2;

                if (response.VirtualGoods.ContainsKey("CUSTOM_VG")) //copy/paste - Tighten this shit up.
                {
                    //virtualItemsAvailable = (int)response.VirtualGoods.GetNumber("CUSTOM_VG");//Check our Virtual Items
                    //ui.vgText.text = virtualItemsAvailable.ToString();
                    int temp = (int)response.VirtualGoods.GetNumber("CUSTOM_VG");
                    //Debug.LogError(temp + " Custom virtual Good");
                }

                if (response.VirtualGoods.ContainsKey("VirtualGood_Item")) //copy/paste - Tighten this shit up.
                {
                    virtualItemsAvailable = (int)response.VirtualGoods.GetNumber("VirtualGood_Item");//Check our Virtual Items
                    ui.vgText.text = virtualItemsAvailable.ToString();
                }
                else
                {
                    virtualItemsAvailable = 0;
                    ui.vgText.text = virtualItemsAvailable.ToString();
                }
            }
            else
            {
                Debug.Log(response.Errors);
             
            }         
        });
    }

    public void UpdateGUI(string name, string uid,string fId)//, string fbId)
    {
        userName = name;
        //userNameLabel.text = userName;
        userId = uid;
        facebookID = fId;
    }

    /*
        public IEnumerator getFBPicture()
        {
            //To get our facebook picture we use this address which we pass our facebookId into
            var www = new WWW("http://graph.facebook.com/" + facebookId + "/picture?width=210&height=210");

            yield return www;

            Texture2D tempPic = new Texture2D(25, 25);

            www.LoadImageIntoTexture(tempPic);
            //profilePicture.mainTexture = tempPic;
        }
        */

    //  MESSAGE DELEGATES ---------------------------------------------------
    void Achievement_MessageHandler(GameSparks.Api.Messages.AchievementEarnedMessage _message) // Could be usefull to parse them out like this...
    {
        Debug.LogError(_message.AchievementName + "-EARNED!!!");

        switch (_message.AchievementName)
        {
            case "HIGHSCORE_ACHV":
                Debug.LogWarning("Awarded-" + _message.AchievementName + "Achievement_MessageHandler");
                break;
            case "NEWPLAYER_ACHV":
                Debug.LogWarning("Awarded-" + _message.AchievementName);
                //NewPlayer_Achievement();
                break;
            default:
                Debug.Log("VertDeFurk??");
                break;
        }
    }

   void HighScore_MessageHandler(GameSparks.Api.Messages.NewHighScoreMessage _message)
   {
       Debug.LogWarning("NEW HIGH SCORE \n " + _message.LeaderboardName);
   }
    /*
       public void MatchMaking()
        {
            new GameSparks.Api.Requests.LogEventRequest().SetEventKey("?").Send((response) => {
                if (!response.HasErrors)
                {
                    Debug.Log("Event sent Successfully...");
                }
                else {
                    Debug.Log("Error calling Event...");
                }
            });
        }

         public void MatchmakingTest()
        {
            new MatchmakingRequest()
           //.SetAction(action)
           //.SetCustomQuery(customQuery)
           //.SetMatchData(matchData)
           //.SetMatchGroup(matchGroup)
           .SetMatchShortCode("MATCH_TEST")
           //.SetParticipantData(participantData)
           //.SetSkill(skill)
           .Send((response) => {

               if (!response.HasErrors)
               {
                   GSData scriptData = response.ScriptData;

               }
               else
               {
                   Debug.Log("Matchmaking Errors!!!!");
               }
           });
        }

    */
    //LeaderBoard Methods---------------------------------------------------------------------------------------------------------------
    public void Set_LeaderBoardData()
    {
        score = int.Parse(ui.scoreInput.text);

        if (score == 0)
        {
            return;
        }

        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("SUB_SCORE").SetEventAttribute("SCORE", score).SetDurable(true).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Score Posted Successfully...");
            }
            else {
                Debug.Log("Error Posting Score...");
            }
        });
    }

    public void Get_LeaderBoardData()//Set Durable- buffers information to set to the server if offline!!!
    {
        //Clear LB UI

        new GameSparks.Api.Requests.LeaderboardDataRequest().SetLeaderboardShortCode("SCORE_BOARD").SetEntryCount(100).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Found Leaderboard Data...");
                foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data)
                {
                    int rank = (int)entry.Rank;
                    string playerName = entry.UserName;
                    string score = entry.JSONData["SCORE"].ToString();
                    Debug.Log("Rank:" + rank + " Name:" + playerName + " \n Score:" + score);

                    //Add LB_Entry prefab to UI here
                }
            }
            else {
                Debug.Log("Error Retrieving Leaderboard Data...");
            }
        });
    }

    //Virtual Goods
    public void Award_Currency()//Add public string Currency type,to handle any currency
    {
        int ammountToAdd = int.Parse(ui.currencyInput.text);

        if (ammountToAdd == 0)
        {
            return;
        }
        else if (ammountToAdd > 0) //Add Currency
        {
            new GameSparks.Api.Requests.LogEventRequest().SetEventKey("GRANT_CURRENCY1").SetEventAttribute("C1_CASH_PLUS", ammountToAdd).SetDurable(true).Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Player recieved " + ammountToAdd + " Successfully...");
                    Get_CurrencyOneBalance();                 
                }
                else {
                    Debug.Log("Error Awarding Currency...");
                }
            });
        }
        else if (ammountToAdd < 0) //Deduct Currency
        {
            new GameSparks.Api.Requests.LogEventRequest().SetEventKey("DEDUCT_CURRENCY1").SetEventAttribute("C1_CASH_MINUS", -ammountToAdd).SetDurable(true).Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Player deducted " + ammountToAdd + " Successfully...");
                    Get_CurrencyOneBalance();
                }
                else {
                    Debug.Log("Error deducting Currency...");
                }
            });
        }

        Get_AccountDetails();
    }

    void Get_CurrencyOneBalance()
    {
        new GameSparks.Api.Requests.AccountDetailsRequest().Send((response) =>
        {
            Debug.Log("I have " + (response.Currency1 != null ? (int)response.Currency1 : 0) + " coins of currency1");
            int playerCurrency = (int)response.Currency1;
            ui.currencyText.text = playerCurrency.ToString();
        });
    }

    public void VirtualGoods_PurchaseTest()
    {
        Buy_VirtualGoods(1, 1, "VirtualGood_Item");
    }

    public void Buy_VirtualGoods(int currencyID, int quantity, string goodsName)
    {
        new GameSparks.Api.Requests.BuyVirtualGoodsRequest().SetCurrencyType(currencyID).SetQuantity(quantity).SetShortCode(goodsName).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Virtual Goods Bought Successfully...");

                GameSparks_ConnectionManager.instance.Player_AccountDetailsRequest();//------------------------------------------Two calls ....................
                Get_AccountDetails();
            }
            else {
                Debug.Log("Error Buying Virtual Goods...");
            }
        });
    }

    public void VirtualGoods_ConsumeTest()
    {
        Consume_VirtualGoods(1, "VirtualGood_Item");
    }

    public void Consume_VirtualGoods(int quantity, string goodsName)
    {
        if (virtualItemsAvailable != 0)
        {
            new GameSparks.Api.Requests.ConsumeVirtualGoodRequest().SetQuantity(quantity).SetShortCode(goodsName).Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Virtual Goods Consumed Successfully...");

                    GameSparks_ConnectionManager.instance.Player_AccountDetailsRequest();//------------------------------------------Two calls ....................
                    Get_AccountDetails();
                }
                else {
                    Debug.Log("Error Consuming Virtual Goods...");
                }
            });
        }
        else
        {
            Debug.LogWarning("You have no VGs to Nom upon.");
        }
   
    }

    public void CUSTOM_VirtualGoods_PurchaseTest()
    {
        TryGet_CUSTOM_VirtualGood( 1, "CUSTOM_VG");
    }

    public void TryGet_CUSTOM_VirtualGood(int quantity, string goodShortCode)
    {
        Debug.Log("TryGet_CUSTOM_VirtualGood!");

        List<string> tagList = new List<string>();
        tagList.Add("CUSTOM");//This is a tag i add to virtual goods that need to be purchased with more than one currency type.

        new ListVirtualGoodsRequest().SetTags(tagList).Send((response)=> //For speed you could cashe this list locally on Authentication for eg.
        {
            if (!response.HasErrors)
            {
                int cost1 = 0;
                int cost2 = 0;

                bool found = false;
                
                foreach (var vg in response.VirtualGoods)//check through our collection of goods
                {
                    Debug.Log(vg.ShortCode.ToString() + "Currency 1 Cost:" + vg.Currency1Cost.ToString() + "Currency 2 Cost" + vg.Currency2Cost.ToString());

                    if (vg.ShortCode == goodShortCode)//Found the one we want
                    {
                        cost1 = (int)vg.Currency1Cost;
                        cost2 = (int)vg.Currency2Cost;
                        found = true;                      
                    }
                }

                if (found)
                {
                    //check currency 1
                    if (playerCurrency1 >= cost1)
                    {
                        Debug.Log("I have enough of Currency 1!");
                    }
                    else
                    {
                        Debug.Log("I DONT have enough of Currency 1.....I only have " + playerCurrency1.ToString());
                    }
                   
                    //check currency 2
                    if (playerCurrency2 >= cost2)
                    {
                        Debug.Log("I have enough of Currency 2!");
                    }
                    else
                    {
                        Debug.Log("I DONT have enough of Currency 2.....I only have " + playerCurrency2.ToString());
                    }

                    //If we have enough of the required currency types
                    if (playerCurrency1 >= cost1 && playerCurrency2 >= cost2)
                    {
                        Debug.Log("I can buy this!");
                        Buy_CUSTOM_VirtualGood(quantity,goodShortCode, cost1, cost2);
                    }
                }
            }
            else
            {
                Debug.LogWarning("List VG failed");
            }
        });
    }

    void Buy_CUSTOM_VirtualGood(int quantity, string goodShortCode, int cost1, int cost2)
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("BUY_CUSTOM_VG")//Call our cloud code event.
            .SetEventAttribute("C_VG_QUANTITY", quantity)
            .SetEventAttribute("C_VG_SHORTCODE", goodShortCode)
            .SetEventAttribute("C_VG_CURRENCY1", cost1)
            .SetEventAttribute("C_VG_CURRENCY2", cost2)
            .SetDurable(true)
            .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.LogWarning("Player recieved " + goodShortCode + " X" + quantity + " Successfully...");
                Get_AccountDetails();
            }
            else
            {
                Debug.LogError(response.Errors.JSON);
            }
        });
    }
}
