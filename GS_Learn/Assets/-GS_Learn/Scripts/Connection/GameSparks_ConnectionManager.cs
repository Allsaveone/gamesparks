﻿using UnityEngine;
using GameSparks.Core;
//using GameSparks.Api;
using GameSparks.Api.Requests;
//using GameSparks.Api.Responses;
//using Facebook;
using System.Collections.Generic;

public class GameSparks_ConnectionManager : MonoBehaviour {

    public static GameSparks_ConnectionManager instance;
    [Header("Connection Info")]
    public bool isConnected = false;
    public bool isAuthenticated = false;
    [Header("Log In")]
    public string displayName;
    public string passWord;
    public string userName;
    public string userId;

    public bool isInFocus;
    UI_Manager ui;

    static string PLAYER_PREF_AUTHTOKEN_KEY = "gamesparks.authtoken";
    static string PLAYER_PREF_USERID_KEY = "gamesparks.userid";
    static string PLAYER_PREF_FBID_KEY = "gamesparks.fbid";

    public bool clearPlayerPrefs = false;
    
    void Awake ()
    {
        //Application.targetFrameRate = 300;

        if (clearPlayerPrefs)
            PlayerPrefs.DeleteAll();// Test Clear --- this will warrant a much more indepth exploration.
           
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            ui = GetComponent<UI_Manager>();

            CheckUserDetails();
        }
        else
        {
            Destroy(this.gameObject);
        }

        Setup_MSG_Delegates();
        InvokeRepeating("CheckConnected", 1f, 1f);
        CallFBInit();
    }

    void Setup_MSG_Delegates()
    {
        GameSparks.Api.Messages.SessionTerminatedMessage.Listener += SessionTerminated_MessageHandler; //bypassed - Message not going through - EXPLORE
    }
/*
    //Push Notifications
    private void sendRegistrationIdToBackend()
    {
        string pushId = "";//filler var

        new PushRegistrationRequest().
                //.SetDeviceOS(deviceOS)
                .SetPushId(pushId)
                .Send((response) => {

                    if (!response.HasErrors)
                    {

                    }
                    else
                    {

                    }

                });
    }
*/
    void CheckUserDetails()
    {
        string authToken = PlayerPrefs.GetString(PLAYER_PREF_AUTHTOKEN_KEY);
        string userId = PlayerPrefs.GetString(PLAYER_PREF_USERID_KEY);
        string fbId = PlayerPrefs.GetString(PLAYER_PREF_FBID_KEY);


        if (fbId == "")
        {
            Debug.LogWarning("No Fb Details found");
            //CallFBInit();//Facebook log in
        }
        else
        {
            Debug.Log("Found Facebook Id: " + fbId);
        }


        if (authToken == "" || userId == "")
        {
            Debug.LogWarning("No Auth/User Details found");
        }
        else
        {
            Debug.Log("Token: " + authToken);
            Debug.Log("User: " + userId);
        }
    }

    bool CheckConnected()
    {
        Debug.LogWarning("Checking Connection...");

        isConnected = GS.Available;
        isAuthenticated = GS.Authenticated; //UI State Change

        if (isConnected && isAuthenticated)
        {
            //Debug.Log("Connected to GAMESPARKS - Account Authenticated");
            if (ui.current_UiState != UI_Manager.uiState.Ingame)
            {
                ui.current_UiState = UI_Manager.uiState.Ingame;
                ui.Switch_UI(ui.current_UiState);

                Player_AccountDetailsRequest();
            }

            Debug.LogWarning("Check Connection Cancelled");
            CancelInvoke("CheckConnected");
        }
        else
        {
            if (ui.current_UiState != UI_Manager.uiState.MainMenu)
            {
                ui.current_UiState = UI_Manager.uiState.MainMenu;
                ui.Switch_UI(ui.current_UiState);
              
            }
        }

        return isConnected;
    }
  
    //Regestration-----------------------------------------------------------------------------------------------------------
    public void RegisterPlayer()
    {
        string name = displayName;
        string pass = passWord;
        string usersName = userName;
        //Debug.Log(displayName + passWord + usersName

        if (userName != string.Empty && displayName != string.Empty && userName != string.Empty)
        {
            new GameSparks.Api.Requests.RegistrationRequest().SetDisplayName(name).SetPassword(pass).SetUserName(usersName).Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Player Registered");

                    if (response.NewPlayer == true)
                    {
                        Debug.Log("New Player!!!!");
                        //Could always do my Newplayer achv here..... Same process at HS_Achv
                    }
                    //Debug.Log(response.Errors.ToString());
                }
                else
                {
                    Debug.Log("Error Registering Player");
                    
                }
            });
        }
        else
        {
            Debug.Log("Blank Details");
        }     
    }

    //Log In ------------------------------------------------------------------------------------------------------
    public void Authenticate_Player()
    {
        Debug.LogWarning("Authenticating...");
        //string name = displayName;
        string usersName = ui.userNameInput.text;
        string pass = ui.passWordInput.text;

        //bool hasUserName = string.IsNullOrEmpty(userName);//Come back here
        //bool hasPassword = string.IsNullOrEmpty(pass);

        if (userName != string.Empty && userName != string.Empty)
        {
            new GameSparks.Api.Requests.AuthenticationRequest().SetUserName(usersName).SetPassword(pass).Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Player Authenticated...");
                    Debug.Log("User DisplayName: " + response.DisplayName.ToString()+"." + " AuthToken: " + response.AuthToken);

                    //sendRegistrationIdToBackend();

                    if (response.NewPlayer == true)
                    {
                        Debug.Log("New Player Authenticated...");
                    }
                    //AuthenticateDevice(usersName);
                }
                else
                {
                    Debug.Log("Error Authenticating Player..." + response.Errors.JSON);
                }

            });
        }
        else
        {
            Debug.Log("Enter Account Details.......");
        }
        
    }

    void AuthenticateDevice(string name)//Makes Log in unique to Device associalted with Players Info
    {
        new GameSparks.Api.Requests.DeviceAuthenticationRequest().SetDisplayName(name).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.LogWarning("Device Authenticated...");
            }
            else
            {
                Debug.LogWarning("Error Authenticating Device...");
            }
        });
    }
    //FB----------------------------------------------------------------------------------------------------------

    private void CallFBInit()
    {
        //When done initialising, call OnInitComplete
        FB.Init(OnInitComplete, OnHideUnity);        
    }

    private void OnInitComplete()
    {
        Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);        
        //CallFBLogin();        
    }

    private void OnHideUnity(bool isGameShown)
    {
        Debug.Log("Is game showing? " + isGameShown);
    }

    public void CallFBLogin()
    {
        //We first tell Facebook what permissions we want and then tell it todo GameSparksLogin when done
        FB.Login("email,user_friends", Authenticate_FaceBook);
    }


    public void Authenticate_FaceBook(FBResult fBresult)//Fb delegeate
    {
        Debug.LogWarning("Attempting FaceBook Login.......");

        if (!GS.Available)
        {
            Debug.LogWarning(GS.Available + "-Reconnecting");

            GS.Reconnect();
        }

        if (FB.IsLoggedIn)
        {
            new GameSparks.Api.Requests.FacebookConnectRequest().SetAccessToken(FB.AccessToken).Send((response) =>
            {
                if (response.HasErrors)
                {
                    Debug.Log("Facebook Login failed..... " + response.Errors.JSON);
                }
                else
                {
                    Debug.Log("Facebook Login Successful!");
                    GameSparks_UserManager.instance.Get_AccountDetails();
                }
            });
        }
    }

    //Account Details Request - Intital data pull on Log in - Further Datapulled by UserManager,
    public void Player_AccountDetailsRequest()
    {
        if (!GS.Available)
        {
            GS.Reconnect();
        }

        Debug.LogWarning("Player_AccountDetailsRequest");

        new GameSparks.Api.Requests.AccountDetailsRequest().Send((response) => {
            if (!response.HasErrors)
            {
                if (response.VirtualGoods.ContainsKey("VirtualGood_Item"))
                {
                   int virtualItemsAvailable = (int)response.VirtualGoods.GetNumber("VirtualGood_Item");//Check our Virtual Items
                    ui.vgText.text = virtualItemsAvailable.ToString();
                }
                else
                {
                    int virtualItemsAvailable = 0;
                    ui.vgText.text = virtualItemsAvailable.ToString();

                }
                //Debug.Log ("Player is in " +response.Location.City + " and has " + virtualItemsAvailable + " VirtualGood_Item");

                List<string> achievementsList = new List<string>();
                achievementsList = response.Achievements; // we can get a list of achievements earned
                // we can grab all the currency-types into a list //
                List<int> currencyList = new List<int>()
                {
                    int.Parse(response.Currency1.ToString()), int.Parse(response.Currency2.ToString()),
                    int.Parse(response.Currency3.ToString()), int.Parse(response.Currency4.ToString()),
                    int.Parse(response.Currency5.ToString()), int.Parse(response.Currency6.ToString()),
                }; // Then we can print this data out or use it wherever we want //

                int playerCurrency = (int)response.Currency1;
                ui.currencyText.text = playerCurrency.ToString();

                //userId = response.UserId;// usefull to have local reference
                GSData data = response.ExternalIds;

                GameSparks_UserManager.instance.UpdateGUI(response.DisplayName, response.UserId,data.GetString("FB"));

                if (data.GetString("FB") != null && PlayerPrefs.GetString(PLAYER_PREF_FBID_KEY) != data.GetString("FB"))
                {
                    PlayerPrefs.SetString(PLAYER_PREF_FBID_KEY, data.GetString("FB"));
                    Debug.LogWarning("Set Facebook id to PlayerPrefs");
                }

                //Get Achievements
                if (achievementsList == null)
                {
                    Debug.Log("No Achievements Earned.");
                }
                else
                {
                    foreach (string a in achievementsList)
                    {
                        Debug.Log("Player Earned: " + a);
                    }
                }

                //Get Currency
                if (currencyList.Count != 0)
                {
                    for (int i = 0; 9 < currencyList.Count; i++)
                    {
                        Debug.Log("Currency" + i + "    " + currencyList[i].ToString());
                    }
                }
                //PlayerPrefs.SetString(PLAYER_PREF_FBID_KEY, response.ExternalIds.JSON);               
            }
            else
            {
                Debug.Log("Error Retrieving Account Details...");
            }
        });
    }

    void SessionTerminated_MessageHandler(GameSparks.Api.Messages.SessionTerminatedMessage _message)
    {
        if (ui.current_UiState != UI_Manager.uiState.MainMenu)
        {
            Debug.LogError("Session Terminated");

            ui.current_UiState = UI_Manager.uiState.MainMenu;
            ui.Switch_UI(ui.current_UiState);
        }
    }

    //Disconnecting and returning to menu screen
    public void LogOut()
    {
        GS.Disconnect();

        if (ui.current_UiState != UI_Manager.uiState.MainMenu)
        {
            Debug.LogWarning("Session Terminated");

            ui.current_UiState = UI_Manager.uiState.MainMenu;
            ui.Switch_UI(ui.current_UiState);
            //could check here to see if we have details saved in player prefs and present them in the Login fields
        }

        Invoke("CheckDisconnect", 0.2f);

        Debug.LogWarning("Check Connection Restarted");
        InvokeRepeating("CheckConnected", 0.25f, 1f);
    }

    void CheckDisconnect()
    {
        //FB.Logout();     
        Debug.Log(CheckConnected().ToString());
    }

    void OnApplicationFocus(bool focus)
    {
        isInFocus = focus;

        if (!isInFocus)
        {
            //Disconnect - Log event- Disconnect in the cloud, safe and will trigger session terminated.
        }
    }

    public void QuitApplication()
    {
        /*
        new LogEventRequest().SetEventKey("Save_EVENT")
        .SetEventAttribute("DATA_TO_SAVE", someData)
        .Send((response) =>{
            if (!response.HasErrors)
            {
                Debug.Log("Data saved from Unity client");
                Debug.Log("Quitting Application");
                Application.Quit();
            }
            else
            {
            Debug.Log(response.Errors.JSON);
            }
        });
        */
        Debug.Log("Quitting Application");
        Application.Quit();
    }

    public void RememberMe_Toggle(bool toggle)
    {
        clearPlayerPrefs = !toggle;
        Debug.Log(clearPlayerPrefs);
    }

}
