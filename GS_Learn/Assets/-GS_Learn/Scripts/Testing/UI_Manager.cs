﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI_Manager : MonoBehaviour {

    public enum uiState
    {
        MainMenu,
        RegisterNewPlayer,
        Ingame
    }

    [Header("UI State")]
    public uiState current_UiState;
    [Header("UI State Groups")]
    public GameObject menuGroup;
    public GameObject inGameGroup;
    //public GameObject logOutButton;
    [Header("MainMenu UI Components")]
    public InputField userNameInput;
    public InputField passWordInput;
    public InputField displayNameInput;
    public Text connectionInfoText;
    public Button AuthenticateButton;
    public Button RegisterButton;
    [Header("InGame UI Components")]
    public InputField scoreInput;
    public InputField currencyInput;
    public Text currencyText;
    public Text vgText;
    [Header("Audio")]
    public AudioClip highScore_SFX;
    public AudioClip button_SFX;

    AudioSource a_Source;

    void Start ()
    {
        a_Source = GetComponent<AudioSource>();

        current_UiState = uiState.MainMenu;
        Switch_UI(current_UiState);
        //Debug.Log((int)uiState.MainMenu);
        
    }

    public void Switch_UI (uiState newState)// Expand Ui funtionality -togglepanels/groups - Basics - Clear clutter
    {
        menuGroup.SetActive(false);
        inGameGroup.SetActive(false);

        switch (current_UiState)
        {
            case uiState.MainMenu:
                menuGroup.SetActive(true);
                break;
            case uiState.RegisterNewPlayer:
                //inGameGroup.SetActive(true);
                break;
            case uiState.Ingame:
                inGameGroup.SetActive(true);
                break;
            default:
                Debug.Log("WTF?");
                break;
        }
    
    }

    public void UI_ButtonAudio()
    {
        if (a_Source.isPlaying)
        {
            a_Source.Stop();
        }

        a_Source.clip = button_SFX;
        a_Source.PlayOneShot(button_SFX);
    }

}
