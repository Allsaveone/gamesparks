﻿using UnityEngine;
using System.Collections;

public class Check_InternetConnection : MonoBehaviour {

    public bool isConnectedToInternet = false;
    public float maxTime = 2.0F;
    public float timeTaken = 0.0F;

    void Awake()
    {
        if(this.isActiveAndEnabled)
        StartCoroutine("ConnectionTest");
    }

    void Update()
    {
        
    }

    IEnumerator ConnectionTest()
    {

        while (true)
        {
            yield return new WaitForSeconds(2f);

            Ping testPing = new Ping("8.8.8.8");//Google
            Debug.Log("Testing......" + testPing.ToString());

            timeTaken = 0.0F;

            while (!testPing.isDone)
            {

                timeTaken += Time.deltaTime;


                if (timeTaken > maxTime)
                {
                    // if time has exceeded the maxtime, break out and return false
                    isConnectedToInternet = false;
                    break;
                }

                yield return null;
            }

            if (timeTaken <= maxTime)
            {
                isConnectedToInternet = true;
            } 
            yield return null;
        }
    }

}
