﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Api.Requests;

public class GameSparks_ChallengeManager : MonoBehaviour
{
    public static GameSparks_ChallengeManager instance;
    [Header("Game Invite")]
    public LayoutGroup inviteGrid;
    public GameObject invitePrefab;
    public List<GameObject> gameInvites = new List<GameObject>();
    [Header("Running Game/s")]
    public LayoutGroup runningGrid;
    public GameObject runningPrefab;
    public List<GameObject> runningGames = new List<GameObject>();
  
    void Start()
    {
        instance = this; 
        Setup_MSG_Delegates();
    }

    void Setup_MSG_Delegates()
    {
        //Issue/Waiting
        GameSparks.Api.Messages.ChallengeIssuedMessage.Listener += ChallengeIssued_MessageHandler;
        GameSparks.Api.Messages.ChallengeWaitingMessage.Listener += ChallengeWaiting_MessageHandler;
        //Decline/Accept
        GameSparks.Api.Messages.ChallengeDeclinedMessage.Listener += ChallengeDeclined_MessageHandler;
        GameSparks.Api.Messages.ChallengeAcceptedMessage.Listener += ChallengeAccepted_MessageHandler;
        //Join/Start
        GameSparks.Api.Messages.ChallengeJoinedMessage.Listener += ChallengeJoined_MessageHandler;
        GameSparks.Api.Messages.ChallengeStartedMessage.Listener += ChallengeStarted_MessageHandler;
        //Withdraw/Draw
        GameSparks.Api.Messages.ChallengeWithdrawnMessage.Listener += ChallengeWithdrawn_MessageHandler;
        GameSparks.Api.Messages.ChallengeDrawnMessage.Listener += ChallengeDrawn_MessageHandler;
        //Win/Loss
        GameSparks.Api.Messages.ChallengeWonMessage.Listener += ChallengeWon_MessageHandler;
        GameSparks.Api.Messages.ChallengeLostMessage.Listener += ChallengeLost_MessageHandler;
        //Custom Messages
        GameSparks.Api.Messages.ScriptMessage_CHAL_MSG.Listener += Chal_MessageHandler;
        GameSparks.Api.Messages.ScriptMessage_DATA_MSG.Listener += Data_MessageHandler;
    }

    //Msg Handlers-------------------------------------------------------------------------------------------------------------
    //Issue/Waiting
    void ChallengeIssued_MessageHandler(GameSparks.Api.Messages.ChallengeIssuedMessage _message)
    {
        Debug.Log("Challenge Issued by " + _message.Challenge.Challenger);
    }

    void ChallengeWaiting_MessageHandler(GameSparks.Api.Messages.ChallengeWaitingMessage _message)
    {
        Debug.Log("Challenge Waiting- " + _message.Challenge.ChallengeName + _message.Challenge.ChallengeId);
    }

    //Decline/Accept
    void ChallengeDeclined_MessageHandler(GameSparks.Api.Messages.ChallengeDeclinedMessage _message)
    {
        Debug.Log(_message.Challenge.ChallengeName + _message.Challenge.ChallengeId + "-Challenge Declined " );
    }

    void ChallengeAccepted_MessageHandler(GameSparks.Api.Messages.ChallengeAcceptedMessage _message)
    {
        Debug.Log(_message.Challenge.ChallengeName + "-Challenge Accepted- Id- " +  _message.Challenge.ChallengeId);
    }

    //Join/Start
    void ChallengeJoined_MessageHandler(GameSparks.Api.Messages.ChallengeJoinedMessage _message)
    {
        Debug.Log("Challenge Joined- " + _message.Challenge.ChallengeName + _message.Challenge.ChallengeId);
    }

    void ChallengeStarted_MessageHandler(GameSparks.Api.Messages.ChallengeStartedMessage _message)
    {
        Debug.LogError("Challenge Started- " + _message.Challenge.ChallengeName + _message.Challenge.ChallengeId);
    }

    //Withdraw/Draw
    void ChallengeWithdrawn_MessageHandler(GameSparks.Api.Messages.ChallengeWithdrawnMessage _message)
    {
        Debug.Log("With Drawning from Challenge- " + _message.Challenge.ChallengeName + _message.Challenge.ChallengeId);
    }

    void ChallengeDrawn_MessageHandler(GameSparks.Api.Messages.ChallengeDrawnMessage _message)
    {
        Debug.Log("Challenge Drawn- " + _message.Challenge.ChallengeName + _message.Challenge.ChallengeId);
    }

    //Win/Loss
    void ChallengeWon_MessageHandler(GameSparks.Api.Messages.ChallengeWonMessage _message)
    {
        Debug.Log("Challenge- " + _message.Challenge.ChallengeName + _message.Challenge.ChallengeId + " Won!");
    }

    void ChallengeLost_MessageHandler(GameSparks.Api.Messages.ChallengeLostMessage _message)
    {
        Debug.Log("Challenge- " + _message.Challenge.ChallengeName + _message.Challenge.ChallengeId + " Lost!");
    }

    //Custom Msgs
    void Chal_MessageHandler(GameSparks.Api.Messages.ScriptMessage_CHAL_MSG _message)
    {
        Debug.Log("Custom SCRIPT_MSG");
    }

    void Data_MessageHandler(GameSparks.Api.Messages.ScriptMessage_DATA_MSG _message)
    {
        Debug.LogWarning("DATA_MSG recieved");       
        Debug.Log( _message.Data.JSON);       
    } 

    //---------------------------------------------------------------------------------------------------------------------
    public void ChallengeUser(List<string> userIds)
    {
        //we use CreateChallengeRequest with the SHORTCODE - NB
        new CreateChallengeRequest()
                .SetAutoStartJoinedChallengeOnMaxPlayers(true)
                .SetChallengeShortCode("CHAL")
                .SetUsersToChallenge(userIds) //userIds of who we want to challenge
                .SetEndTime(System.DateTime.Today.AddDays(1)) //date and time the challenge will end on
                .SetChallengeMessage("I've challenged you!") //message along with the invite
                .SetMaxAttempts(16)//max turns allowed for this challenge
                .SetMaxPlayers(4)
                .SetMinPlayers(4)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        Debug.Log("Challenge Made!");
                    }
                    else
                    {
                        Debug.Log(response.Errors.JSON);
                    }
                });
    }

    public void GetChallengeInvites()
    {
        Debug.Log("GetChallengeInvites");

        for (int i = 0; i < gameInvites.Count; i++)
        {
            Destroy(gameInvites[i]);
        }

        gameInvites.Clear();

        new ListChallengeRequest().SetShortCode("CHAL")
                .SetState("RECEIVED") //challenges status
                .SetEntryCount(50) //first 50
                .Send((response) =>
                {
                    Debug.Log(response.ChallengeInstances.ToString());

                    if (!response.HasErrors)
                    {
                        foreach (var challenge in response.ChallengeInstances)
                        {
                            Debug.Log("Invite Found- " + challenge.ChallengeId);

                            if (challenge.Challenger.Id == GameSparks_UserManager.instance.userId)//If i challenge myself...
                            {
                                Debug.Log("I created this Challenge");
                            }
                            else
                            {
                                Debug.Log("Invite Found " + challenge.Challenger.Name.ToString());
                                ///*
                                GameObject go = Instantiate(invitePrefab, inviteGrid.transform.position, Quaternion.identity) as GameObject;
                                //Update all the gameObject's variables
                                go.GetComponent<GameSparks_GameInvite>().challengeId = challenge.ChallengeId;
                                go.GetComponent<GameSparks_GameInvite>().inviteName = challenge.Challenger.Name;
                                go.GetComponent<GameSparks_GameInvite>().inviteExpiry = challenge.EndDate.ToString();

                                //Add the gameObject to the list of friends
                                gameInvites.Add(go);

                                //Tell the grid to reposition everything appropriatly
                                go.transform.SetParent(inviteGrid.transform, false);
                                // */
                            }
                        }
                    }
                    else
                    {
                        Debug.Log(response.Errors);
                    }             
                });
    }

    public void GetRunningChallenges()
    {
        for (int i = 0; i < runningGames.Count; i++)
        {
            Destroy(runningGames[i]);
        }
        runningGames.Clear();

        Debug.Log("Getting Running Challenges");

        //We send a ListChallenge Request with the shortcode of the challenge
        new ListChallengeRequest().SetShortCode("CHAL")
            .SetState("RUNNING") //all games that are running
            .SetEntryCount(50) 
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    //For every challenge we receive
                    foreach (var challenge in response.ChallengeInstances)
                    {
                        Debug.Log("Running Game -" + challenge.ChallengeId + "-Found");

                        GameObject go = Instantiate(runningPrefab, runningGrid.transform.position, Quaternion.identity) as GameObject;

                        //Set our variables
                        go.GetComponent<GameSparks_RunningGame>().challengeId = challenge.ChallengeId;

                        //For every player in the collection of players who have accepted the challenge
                        foreach (var player in challenge.Accepted)
                        {
                            //Add their names and their Ids to the list i each respective Running Game Entry
                            go.GetComponent<GameSparks_RunningGame>().playerNames.Add(player.Name);
                            go.GetComponent<GameSparks_RunningGame>().playerIds.Add(player.Id);
                        }

                        go.GetComponent<GameSparks_RunningGame>().challengerId = challenge.Challenger.Id;
                        go.GetComponent<GameSparks_RunningGame>().statusLabel.text = challenge.State;

                        runningGames.Add(go);
                        go.transform.SetParent(runningGrid.transform, true);
                    }
                }
                else
                {
                    Debug.Log(response.Errors.JSON);
                }              
            });
    }

    public void Send_DataMessage()
    {
        string id = ""; //Player id- This can also be passed an array of Ids/
        string someData = "I r data lolz";
        string someJson = JsonUtility.ToJson(someData);//Eg of some Data

        new LogEventRequest().SetEventKey("DATA_MSG_EVENT")
            .SetEventAttribute("SEND_TO_PID",id)
            .SetEventAttribute("DATA_TO_SEND", someData)
            .Send((response) => 
        
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Data mesg sent from Unity client");
                }
                else
                {
                    Debug.Log(response.Errors.JSON);
                }
            });
    }

    public void LaunchExternal_AndroidAPP(string appName)
    {
        string bundleId = "com.google." + appName; // your target bundle id
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); //Androids unity player
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");//the players current activity
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");// manager to launch our new app

        //local vars for our attempt
        bool fail = false;
        AndroidJavaObject launchIntent = null;
        //Attempt the launch
        try
        {
            launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
        }
        catch (System.Exception e)
        {
            fail = true;
            e.ToString();
        }

        if (fail)//
        {
            Debug.Log("Launcher Failed");
        }
        else //open the app
        {
            ca.Call("startActivity", launchIntent);
        }

        //Garbage disposal
        up.Dispose();
        ca.Dispose();
        packageManager.Dispose();
        launchIntent.Dispose();
    }
}
