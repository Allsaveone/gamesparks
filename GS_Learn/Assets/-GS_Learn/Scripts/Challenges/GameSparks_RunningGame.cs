﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameSparks_RunningGame : MonoBehaviour {

    public string challengeId, challengerId, turnStatus;

    public List<string> playerNames = new List<string>();
    public List<string> playerIds = new List<string>();

    public bool canDestroy = false;
    public Text challengeLabel, statusLabel;

    void Start()
    {    
        challengeLabel.text = playerNames[0] + " VS " + playerNames[1];
    }

    public void StartGame()
    {
        Debug.Log(challengeLabel.text + " Started");
       
        GameSparks_Challenge.challengeInstance.ClearInstance();

        //if (statusLabel.text != "RUNNING")
        //{
            GameSparks_Challenge.challengeInstance.challengeInstanceId = challengeId; // need to add a check here 
        //}
        //else
       //{
       //     Debug.Log("Already Running!");
       // }

        //Set Challenge Variables.
        ///*
        //*/

        //remove entry from the scene.
        canDestroy = true;
    }

    public void DestroyAfterTween()
    {
        if (canDestroy)
        {
            GameSparks_ChallengeManager.instance.gameInvites.Remove(gameObject);
            Destroy(gameObject);
        }
    }
}
