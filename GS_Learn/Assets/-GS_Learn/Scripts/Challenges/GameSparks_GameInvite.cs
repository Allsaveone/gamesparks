﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GameSparks.Api.Requests;

public class GameSparks_GameInvite : MonoBehaviour {

    [Header("Invite Info")]//ChallengeId NB
    public string inviteName, inviteExpiry, challengeId;// , facebookId;
    [Header("Invite Ui")]
    public Text inviteNameLabel, inviteExpiryLabel;
    public Image profilePicture;
    //Anim/CleanUp Bool
    public bool canDestroy = false;

    void Start()
    {
        inviteNameLabel.text = inviteName + "has invited you to play";
        inviteExpiryLabel.text = "Expires on " + inviteExpiry;
    }

    public void AcceptChallenge_Button()
    {
        //We set the Challenge Instance Id and Message of AcceptChallengeRequest
        new AcceptChallengeRequest().SetChallengeInstanceId(challengeId)
                .SetMessage("Challenge Accepted!")
                .Send((response) =>
                {
                    if (response.HasErrors)
                    {
                        Debug.Log(response.Errors);
                    }
                    else
                    {
                        //Since this challenge is no longer an invite, we need to update our running games
                        GameSparks_ChallengeManager.instance.GetRunningChallenges();
                        //Once we accept the challenge we can go ahead and remove the gameinvite
                        canDestroy = true;
                        DestroyAfter();
                    }
                });
    }

    public void DeclineChallenge_Button()
    {
        new DeclineChallengeRequest().SetChallengeInstanceId(challengeId)
                .SetMessage("Challenge Declined!")
                .Send((response) =>
                {
                    if (response.HasErrors)
                    {
                        Debug.Log(response.Errors);
                    }
                    else
                    {
                        //declined challenge,remove the gameInvite
                        canDestroy = true;
                        DestroyAfter();
                    }
                });
    }
/*
    public IEnumerator getFBPicture()
    {
        //To get our facebook picture we use this address which we pass our facebookId into
        var www = new WWW("http://graph.facebook.com/" + facebookId + "/picture?width=210&height=210");

        yield return www;

        Texture2D tempPic = new Texture2D(25, 25);

        www.LoadImageIntoTexture(tempPic);
        //profilePicture.mainTexture = tempPic;
    }
*/
    public void DestroyAfter()
    {
        if (canDestroy)
        {
            //Fade out? animation etc
            GameSparks_ChallengeManager.instance.gameInvites.Remove(gameObject);
            Destroy(gameObject);
        }
    }
}
