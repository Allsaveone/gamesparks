﻿using UnityEngine;
using System.Collections;

public class GameSparks_Challenge : MonoBehaviour {

    private static GameSparks_Challenge _challengeInstance;
    public static GameSparks_Challenge challengeInstance //Nifty
    {
        get
        {
            if (_challengeInstance == null)
            {
                _challengeInstance = GameObject.FindObjectOfType<GameSparks_Challenge>();
            }
            return _challengeInstance;
        }
    }

    [Header("Challenge Info")]
    public string challengeInstanceId;
    [Header("Game/Challenge State")]
    public bool gameOver = false;

    public void SetUp_Challenge()// Set up the scene/Challenge
    {

    }

    public void Challenge_Outcome()//Check Win/Loss
    {

    }

    public void Challenge_Lost()//Losers
    {

    }

    public void Challenge_Won()//Winners
    {

    }

    //Reset to a blank state
    public void ClearInstance()
    {
        Debug.Log("Cleared Challenege Instance");
        gameOver = false;
    }
}
