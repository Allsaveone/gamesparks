﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Api.Requests;

public class GameSparks_FriendsManager : MonoBehaviour
{

    public GameObject friendEntryPrefab;
    public Transform friendGrid;
    public List<GameObject> friends = new List<GameObject>();

    GameObject new_FriendEntry;
    public bool toggleFriendList = false;

    //Need a togglefor FList
    public void Get_Friends()
    {
        toggleFriendList = !toggleFriendList;

        if (toggleFriendList)
        {
            for (int i = 0; i < friends.Count; i++)
            {
                Destroy(friends[i]);
            }

            friends.Clear();

            new ListGameFriendsRequest().Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Transform parent = GameObject.Find("FriendList_Layout").transform;

                    if (response.Friends != null)
                    {
                        foreach (var friend in response.Friends)
                        {
                            new_FriendEntry = Instantiate(friendEntryPrefab, parent.position, Quaternion.identity) as GameObject;
                            GameSparks_FriendEntry entry = new_FriendEntry.GetComponent<GameSparks_FriendEntry>();
                            entry.displayName = friend.DisplayName;
                            entry.id = friend.Id;
                            entry.isOnline = friend.Online.Value;

                            new_FriendEntry.name = "Friend_Entry";
                            friends.Add(new_FriendEntry);

                            new_FriendEntry.transform.SetParent(parent, false);
                        }
                        //Debug.Log(friends.Count);
                    }
                }
                else
                {
                    Debug.Log("Error retrieving Friends List");
                }

            });
        }
        else
        {
            for (int i = 0; i < friends.Count; i++)
            {
                Destroy(friends[i]);
            }

            friends.Clear();
        }      
    }
    
    // Getting Ids??? -- Add "ingame"? 
    public void Friend_Request(List<string> userId)
    {
        //I Challenge you to FRIENDSHIP!!!!
        new CreateChallengeRequest().SetChallengeShortCode("F_REQUEST")
                .SetUsersToChallenge(userId) 
                .SetEndTime(System.DateTime.Today.AddDays(1)) 
                .SetChallengeMessage(GameSparks_ConnectionManager.instance.displayName + "has sent you a Friend Request") 
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        Debug.Log("FriendRequest sent");
                    }
                    else
                    {
                        Debug.Log("FriendRequest Failed -" + response.Errors);
                    }
                });
    }

    public void Delete_Friend(string friendId)
    {
        new LogChallengeEventRequest_F_REMOVE().Set_F_P_ID(friendId).Send((response) =>
            {
                if(!response.HasErrors)
                {
                    Debug.Log("Friend removed !");
                }
                else
                {
                    Debug.LogWarning("Error Removing Friend");
                }
            });
    }
}