﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameSparks_FriendEntry : MonoBehaviour
{
    [Header("Friend Info")]
    public string displayName, id;//, facebookId;
    public bool isOnline;
    [Header("Friend Ui")]
    public Text nameLabel;
    public Image profilePicture;
    public Image onlineImage;

    void Start()
    {
         UpdateFriend();
    }

    public void UpdateFriend()
    {
        nameLabel.text = displayName;
        onlineImage.color = isOnline ? Color.blue : Color.gray;
        //StartCoroutine(getFBPicture());
    }
    /*
        public IEnumerator getFBPicture()
        {
            //To get our facebook picture we use this address which we pass our facebookId into
            var www = new WWW("http://graph.facebook.com/" + facebookId + "/picture?width=210&height=210");

            yield return www;

            Texture2D tempPic = new Texture2D(25, 25);

            www.LoadImageIntoTexture(tempPic);
            //profilePicture.mainTexture = tempPic;
        }
    */

    public void StartChallenge()
    {
        //Debug.Log("StartChallenge");
        //CreateChallengeRequest takes a list of UserIds because you can challenge more than one user at a time
        List<string> gsId = new List<string>();
        //Add our friends UserId to the list
        gsId.Add(id);
        //Call ChallengeUser 
        GameSparks_ChallengeManager.instance.ChallengeUser(gsId);
    }
}