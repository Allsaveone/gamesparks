using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
	public class LogEventRequest_AWARD_CLOUD_ACHV : GSTypedRequest<LogEventRequest_AWARD_CLOUD_ACHV, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_AWARD_CLOUD_ACHV() : base("LogEventRequest"){
			request.AddString("eventKey", "AWARD_CLOUD_ACHV");
		}
		public LogEventRequest_AWARD_CLOUD_ACHV Set_C1_CASH( long value )
		{
			request.AddNumber("C1_CASH", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_AWARD_CLOUD_ACHV : GSTypedRequest<LogChallengeEventRequest_AWARD_CLOUD_ACHV, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_AWARD_CLOUD_ACHV() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "AWARD_CLOUD_ACHV");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_AWARD_CLOUD_ACHV SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_AWARD_CLOUD_ACHV Set_C1_CASH( long value )
		{
			request.AddNumber("C1_CASH", value);
			return this;
		}			
	}
	
	public class LogEventRequest_AWARD_HS_ACHV : GSTypedRequest<LogEventRequest_AWARD_HS_ACHV, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_AWARD_HS_ACHV() : base("LogEventRequest"){
			request.AddString("eventKey", "AWARD_HS_ACHV");
		}
	}
	
	public class LogChallengeEventRequest_AWARD_HS_ACHV : GSTypedRequest<LogChallengeEventRequest_AWARD_HS_ACHV, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_AWARD_HS_ACHV() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "AWARD_HS_ACHV");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_AWARD_HS_ACHV SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_AWARD_NEWPLAYER_ACHV : GSTypedRequest<LogEventRequest_AWARD_NEWPLAYER_ACHV, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_AWARD_NEWPLAYER_ACHV() : base("LogEventRequest"){
			request.AddString("eventKey", "AWARD_NEWPLAYER_ACHV");
		}
	}
	
	public class LogChallengeEventRequest_AWARD_NEWPLAYER_ACHV : GSTypedRequest<LogChallengeEventRequest_AWARD_NEWPLAYER_ACHV, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_AWARD_NEWPLAYER_ACHV() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "AWARD_NEWPLAYER_ACHV");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_AWARD_NEWPLAYER_ACHV SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_CHAL_ACCEPT : GSTypedRequest<LogEventRequest_CHAL_ACCEPT, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_CHAL_ACCEPT() : base("LogEventRequest"){
			request.AddString("eventKey", "CHAL_ACCEPT");
		}
		
		public LogEventRequest_CHAL_ACCEPT Set_CHAL_ID( string value )
		{
			request.AddString("CHAL_ID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_CHAL_ACCEPT : GSTypedRequest<LogChallengeEventRequest_CHAL_ACCEPT, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_CHAL_ACCEPT() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "CHAL_ACCEPT");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_CHAL_ACCEPT SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_CHAL_ACCEPT Set_CHAL_ID( string value )
		{
			request.AddString("CHAL_ID", value);
			return this;
		}
	}
	
	public class LogEventRequest_CHAL_DECLINE : GSTypedRequest<LogEventRequest_CHAL_DECLINE, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_CHAL_DECLINE() : base("LogEventRequest"){
			request.AddString("eventKey", "CHAL_DECLINE");
		}
		
		public LogEventRequest_CHAL_DECLINE Set_CHAL_ID( string value )
		{
			request.AddString("CHAL_ID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_CHAL_DECLINE : GSTypedRequest<LogChallengeEventRequest_CHAL_DECLINE, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_CHAL_DECLINE() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "CHAL_DECLINE");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_CHAL_DECLINE SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_CHAL_DECLINE Set_CHAL_ID( string value )
		{
			request.AddString("CHAL_ID", value);
			return this;
		}
	}
	
	public class LogEventRequest_CUS_MSG_EVENT : GSTypedRequest<LogEventRequest_CUS_MSG_EVENT, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_CUS_MSG_EVENT() : base("LogEventRequest"){
			request.AddString("eventKey", "CUS_MSG_EVENT");
		}
	}
	
	public class LogChallengeEventRequest_CUS_MSG_EVENT : GSTypedRequest<LogChallengeEventRequest_CUS_MSG_EVENT, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_CUS_MSG_EVENT() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "CUS_MSG_EVENT");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_CUS_MSG_EVENT SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_DATA_MSG_EVENT : GSTypedRequest<LogEventRequest_DATA_MSG_EVENT, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_DATA_MSG_EVENT() : base("LogEventRequest"){
			request.AddString("eventKey", "DATA_MSG_EVENT");
		}
		
		public LogEventRequest_DATA_MSG_EVENT Set_SEND_TO_PID( string value )
		{
			request.AddString("SEND_TO_PID", value);
			return this;
		}
		public LogEventRequest_DATA_MSG_EVENT Set_DATA_TO_SEND( GSData value )
		{
			request.AddObject("DATA_TO_SEND", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_DATA_MSG_EVENT : GSTypedRequest<LogChallengeEventRequest_DATA_MSG_EVENT, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_DATA_MSG_EVENT() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "DATA_MSG_EVENT");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_DATA_MSG_EVENT SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_DATA_MSG_EVENT Set_SEND_TO_PID( string value )
		{
			request.AddString("SEND_TO_PID", value);
			return this;
		}
		public LogChallengeEventRequest_DATA_MSG_EVENT Set_DATA_TO_SEND( GSData value )
		{
			request.AddObject("DATA_TO_SEND", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_DEDUCT_CURRENCY1 : GSTypedRequest<LogEventRequest_DEDUCT_CURRENCY1, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_DEDUCT_CURRENCY1() : base("LogEventRequest"){
			request.AddString("eventKey", "DEDUCT_CURRENCY1");
		}
		public LogEventRequest_DEDUCT_CURRENCY1 Set_C1_CASH_MINUS( long value )
		{
			request.AddNumber("C1_CASH_MINUS", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_DEDUCT_CURRENCY1 : GSTypedRequest<LogChallengeEventRequest_DEDUCT_CURRENCY1, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_DEDUCT_CURRENCY1() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "DEDUCT_CURRENCY1");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_DEDUCT_CURRENCY1 SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_DEDUCT_CURRENCY1 Set_C1_CASH_MINUS( long value )
		{
			request.AddNumber("C1_CASH_MINUS", value);
			return this;
		}			
	}
	
	public class LogEventRequest_F_FIND : GSTypedRequest<LogEventRequest_F_FIND, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_F_FIND() : base("LogEventRequest"){
			request.AddString("eventKey", "F_FIND");
		}
		
		public LogEventRequest_F_FIND Set_DISPLAY_NAME( string value )
		{
			request.AddString("DISPLAY_NAME", value);
			return this;
		}
		public LogEventRequest_F_FIND Set_ENTRY_COUNT( long value )
		{
			request.AddNumber("ENTRY_COUNT", value);
			return this;
		}			
		public LogEventRequest_F_FIND Set_OFFSET( long value )
		{
			request.AddNumber("OFFSET", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_F_FIND : GSTypedRequest<LogChallengeEventRequest_F_FIND, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_F_FIND() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "F_FIND");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_F_FIND SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_F_FIND Set_DISPLAY_NAME( string value )
		{
			request.AddString("DISPLAY_NAME", value);
			return this;
		}
		public LogChallengeEventRequest_F_FIND Set_ENTRY_COUNT( long value )
		{
			request.AddNumber("ENTRY_COUNT", value);
			return this;
		}			
		public LogChallengeEventRequest_F_FIND Set_OFFSET( long value )
		{
			request.AddNumber("OFFSET", value);
			return this;
		}			
	}
	
	public class LogEventRequest_F_REMOVE : GSTypedRequest<LogEventRequest_F_REMOVE, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_F_REMOVE() : base("LogEventRequest"){
			request.AddString("eventKey", "F_REMOVE");
		}
		
		public LogEventRequest_F_REMOVE Set_F_P_ID( string value )
		{
			request.AddString("F_P_ID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_F_REMOVE : GSTypedRequest<LogChallengeEventRequest_F_REMOVE, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_F_REMOVE() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "F_REMOVE");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_F_REMOVE SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_F_REMOVE Set_F_P_ID( string value )
		{
			request.AddString("F_P_ID", value);
			return this;
		}
	}
	
	public class LogEventRequest_F_REQUEST_ACCEPT : GSTypedRequest<LogEventRequest_F_REQUEST_ACCEPT, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_F_REQUEST_ACCEPT() : base("LogEventRequest"){
			request.AddString("eventKey", "F_REQUEST_ACCEPT");
		}
		
		public LogEventRequest_F_REQUEST_ACCEPT Set_F_REQUEST_ID( string value )
		{
			request.AddString("F_REQUEST_ID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_F_REQUEST_ACCEPT : GSTypedRequest<LogChallengeEventRequest_F_REQUEST_ACCEPT, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_F_REQUEST_ACCEPT() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "F_REQUEST_ACCEPT");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_F_REQUEST_ACCEPT SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_F_REQUEST_ACCEPT Set_F_REQUEST_ID( string value )
		{
			request.AddString("F_REQUEST_ID", value);
			return this;
		}
	}
	
	public class LogEventRequest_F_REQUEST_DECLINE : GSTypedRequest<LogEventRequest_F_REQUEST_DECLINE, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_F_REQUEST_DECLINE() : base("LogEventRequest"){
			request.AddString("eventKey", "F_REQUEST_DECLINE");
		}
		
		public LogEventRequest_F_REQUEST_DECLINE Set_F_REQUEST_ID( string value )
		{
			request.AddString("F_REQUEST_ID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_F_REQUEST_DECLINE : GSTypedRequest<LogChallengeEventRequest_F_REQUEST_DECLINE, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_F_REQUEST_DECLINE() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "F_REQUEST_DECLINE");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_F_REQUEST_DECLINE SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_F_REQUEST_DECLINE Set_F_REQUEST_ID( string value )
		{
			request.AddString("F_REQUEST_ID", value);
			return this;
		}
	}
	
	public class LogEventRequest_GET_CURRENCYBALANCE : GSTypedRequest<LogEventRequest_GET_CURRENCYBALANCE, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GET_CURRENCYBALANCE() : base("LogEventRequest"){
			request.AddString("eventKey", "GET_CURRENCYBALANCE");
		}
		public LogEventRequest_GET_CURRENCYBALANCE Set_C_ID( long value )
		{
			request.AddNumber("C_ID", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_GET_CURRENCYBALANCE : GSTypedRequest<LogChallengeEventRequest_GET_CURRENCYBALANCE, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GET_CURRENCYBALANCE() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GET_CURRENCYBALANCE");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GET_CURRENCYBALANCE SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_GET_CURRENCYBALANCE Set_C_ID( long value )
		{
			request.AddNumber("C_ID", value);
			return this;
		}			
	}
	
	public class LogEventRequest_GET_POS : GSTypedRequest<LogEventRequest_GET_POS, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GET_POS() : base("LogEventRequest"){
			request.AddString("eventKey", "GET_POS");
		}
	}
	
	public class LogChallengeEventRequest_GET_POS : GSTypedRequest<LogChallengeEventRequest_GET_POS, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GET_POS() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GET_POS");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GET_POS SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_GRANT_CURRENCY1 : GSTypedRequest<LogEventRequest_GRANT_CURRENCY1, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GRANT_CURRENCY1() : base("LogEventRequest"){
			request.AddString("eventKey", "GRANT_CURRENCY1");
		}
		public LogEventRequest_GRANT_CURRENCY1 Set_C1_CASH_PLUS( long value )
		{
			request.AddNumber("C1_CASH_PLUS", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_GRANT_CURRENCY1 : GSTypedRequest<LogChallengeEventRequest_GRANT_CURRENCY1, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GRANT_CURRENCY1() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GRANT_CURRENCY1");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GRANT_CURRENCY1 SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_GRANT_CURRENCY1 Set_C1_CASH_PLUS( long value )
		{
			request.AddNumber("C1_CASH_PLUS", value);
			return this;
		}			
	}
	
	public class LogEventRequest_SET_POS : GSTypedRequest<LogEventRequest_SET_POS, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_SET_POS() : base("LogEventRequest"){
			request.AddString("eventKey", "SET_POS");
		}
		public LogEventRequest_SET_POS Set_POS( GSData value )
		{
			request.AddObject("POS", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_SET_POS : GSTypedRequest<LogChallengeEventRequest_SET_POS, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_SET_POS() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "SET_POS");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_SET_POS SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_SET_POS Set_POS( GSData value )
		{
			request.AddObject("POS", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_SUB_SCORE : GSTypedRequest<LogEventRequest_SUB_SCORE, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_SUB_SCORE() : base("LogEventRequest"){
			request.AddString("eventKey", "SUB_SCORE");
		}
		public LogEventRequest_SUB_SCORE Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_SUB_SCORE : GSTypedRequest<LogChallengeEventRequest_SUB_SCORE, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_SUB_SCORE() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "SUB_SCORE");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_SUB_SCORE SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_SUB_SCORE Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
	}
	
}
	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_SCORE_BOARD : GSTypedRequest<LeaderboardDataRequest_SCORE_BOARD,LeaderboardDataResponse_SCORE_BOARD>
	{
		public LeaderboardDataRequest_SCORE_BOARD() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "SCORE_BOARD");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_SCORE_BOARD (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_SCORE_BOARD SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_SCORE_BOARD : GSTypedRequest<AroundMeLeaderboardRequest_SCORE_BOARD,AroundMeLeaderboardResponse_SCORE_BOARD>
	{
		public AroundMeLeaderboardRequest_SCORE_BOARD() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "SCORE_BOARD");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_SCORE_BOARD (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_SCORE_BOARD SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SCORE_BOARD SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SCORE_BOARD SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SCORE_BOARD SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SCORE_BOARD SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SCORE_BOARD SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SCORE_BOARD SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_SCORE_BOARD : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_SCORE_BOARD(GSData data) : base(data){}
		public long? SCORE{
			get{return response.GetNumber("SCORE");}
		}
	}
	
	public class LeaderboardDataResponse_SCORE_BOARD : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_SCORE_BOARD(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SCORE_BOARD> Data_SCORE_BOARD{
			get{return new GSEnumerable<_LeaderboardEntry_SCORE_BOARD>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SCORE_BOARD(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCORE_BOARD> First_SCORE_BOARD{
			get{return new GSEnumerable<_LeaderboardEntry_SCORE_BOARD>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SCORE_BOARD(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCORE_BOARD> Last_SCORE_BOARD{
			get{return new GSEnumerable<_LeaderboardEntry_SCORE_BOARD>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SCORE_BOARD(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_SCORE_BOARD : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_SCORE_BOARD(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SCORE_BOARD> Data_SCORE_BOARD{
			get{return new GSEnumerable<_LeaderboardEntry_SCORE_BOARD>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SCORE_BOARD(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCORE_BOARD> First_SCORE_BOARD{
			get{return new GSEnumerable<_LeaderboardEntry_SCORE_BOARD>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SCORE_BOARD(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCORE_BOARD> Last_SCORE_BOARD{
			get{return new GSEnumerable<_LeaderboardEntry_SCORE_BOARD>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SCORE_BOARD(data);});}
		}
	}
}	

namespace GameSparks.Api.Messages {

		public class ScriptMessage_CHAL_MSG : ScriptMessage {
		
			public new static Action<ScriptMessage_CHAL_MSG> Listener;
	
			public ScriptMessage_CHAL_MSG(GSData data) : base(data){}
	
			private static ScriptMessage_CHAL_MSG Create(GSData data)
			{
				ScriptMessage_CHAL_MSG message = new ScriptMessage_CHAL_MSG (data);
				return message;
			}
	
			static ScriptMessage_CHAL_MSG()
			{
				handlers.Add (".ScriptMessage_CHAL_MSG", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_CUS_MSG : ScriptMessage {
		
			public new static Action<ScriptMessage_CUS_MSG> Listener;
	
			public ScriptMessage_CUS_MSG(GSData data) : base(data){}
	
			private static ScriptMessage_CUS_MSG Create(GSData data)
			{
				ScriptMessage_CUS_MSG message = new ScriptMessage_CUS_MSG (data);
				return message;
			}
	
			static ScriptMessage_CUS_MSG()
			{
				handlers.Add (".ScriptMessage_CUS_MSG", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_DATA_MSG : ScriptMessage {
		
			public new static Action<ScriptMessage_DATA_MSG> Listener;
	
			public ScriptMessage_DATA_MSG(GSData data) : base(data){}
	
			private static ScriptMessage_DATA_MSG Create(GSData data)
			{
				ScriptMessage_DATA_MSG message = new ScriptMessage_DATA_MSG (data);
				return message;
			}
	
			static ScriptMessage_DATA_MSG()
			{
				handlers.Add (".ScriptMessage_DATA_MSG", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_F_REQUEST_ACCEPTED_MSG : ScriptMessage {
		
			public new static Action<ScriptMessage_F_REQUEST_ACCEPTED_MSG> Listener;
	
			public ScriptMessage_F_REQUEST_ACCEPTED_MSG(GSData data) : base(data){}
	
			private static ScriptMessage_F_REQUEST_ACCEPTED_MSG Create(GSData data)
			{
				ScriptMessage_F_REQUEST_ACCEPTED_MSG message = new ScriptMessage_F_REQUEST_ACCEPTED_MSG (data);
				return message;
			}
	
			static ScriptMessage_F_REQUEST_ACCEPTED_MSG()
			{
				handlers.Add (".ScriptMessage_F_REQUEST_ACCEPTED_MSG", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_F_REQUEST_MSG : ScriptMessage {
		
			public new static Action<ScriptMessage_F_REQUEST_MSG> Listener;
	
			public ScriptMessage_F_REQUEST_MSG(GSData data) : base(data){}
	
			private static ScriptMessage_F_REQUEST_MSG Create(GSData data)
			{
				ScriptMessage_F_REQUEST_MSG message = new ScriptMessage_F_REQUEST_MSG (data);
				return message;
			}
	
			static ScriptMessage_F_REQUEST_MSG()
			{
				handlers.Add (".ScriptMessage_F_REQUEST_MSG", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}

}
